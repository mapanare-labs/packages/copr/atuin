%global debug_package %{nil}

Name:    atuin
Version:        18.3.0
Release: 1%{?dist}
Summary: Bun is an all-in-one toolkit for JavaScript and TypeScript apps.
Group:   Applications/System
License: MIT
Url:     https://github.com/atuinsh/%{name}
Source0: %{url}/releases/download/v%{version}/%{name}-v%{version}-x86_64-unknown-linux-gnu.tar.gz

%description
Magical shell history.

%prep
%setup -qn %{name}-v%{version}-x86_64-unknown-linux-gnu

%install
install -d -m 755 %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}

%files
%{_bindir}/%{name}
%license LICENSE
%doc README.md

%changelog
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Apr 11 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v18.0.2
* Wed Dec 27 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
